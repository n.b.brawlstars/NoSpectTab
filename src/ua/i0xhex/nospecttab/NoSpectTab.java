package ua.i0xhex.nospecttab;

import org.bukkit.plugin.java.JavaPlugin;

import com.comphenix.protocol.ProtocolLibrary;

public class NoSpectTab extends JavaPlugin {
	private PacketListener packetListener;
	
	@Override
	public void onEnable() {
		packetListener = new PacketListener(this);
		ProtocolLibrary.getProtocolManager().addPacketListener(packetListener);
	}
	
	@Override
	public void onDisable() {
		ProtocolLibrary.getProtocolManager().removePacketListener(packetListener);
	}
}